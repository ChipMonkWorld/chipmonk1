Chipmonk Core
=====================================

**[Website](https://chipmonk.world/ ) | [Explorer](https://explorer.chipmonk.world/) | [Bitcointalk Ann](https://bitcointalk.org/index.php?topic=5360574.0) | [Twitter](https://twitter.com/ChipmonkWorldhttps://bitcointalk.org/index.php?topic=5360574.0) | [OpenSea Collection](https://opensea.io/collection/chipmonk-world)| [Exchange](https://main.southxchange.com/Market/Book/CPMK/BTC)** |[Discord](https://discord.gg/ES5hTVF77R)**


What is Chipmonk?
------------------

Chipmonk is an experimental digital currency that enables instant payments to
anyone, anywhere in the world. Chipmonk uses peer-to-peer technology to operate
with no central authority: managing transactions and issuing money are carried
out collectively by the network. Chipmonk Core is the name of open source
software which enables the use of this currency.

ChipMonk World is a collection of 10,000 unique NFT's living on the Ethereum blockchain. 
Each ChipMonk is holding different attributes and powers from a total of 150 traits over 17 categories. 
The ChipMonk grants you access to a members-only Harvesting Game with cash rewards and other benefits.

For more information, as well as an immediately useable, binary version of
the Chipmonk Core software, **[see Release](https://gitlab.com/ChipMonkWorld/chipmonk1)**.

Specifications
------

| Specification          | Value                  |
| ---------------------- |:-----------------------|
| Algo                   | Yespower           |
| Premine                | 6849500                      |
| Block Spacing          | 300 seconds             |
| Max Supply             | 21,000,000 (21 Million)|
| Block Reward           | 1 - 0.25  CPMK           |
| Block Reward           | 10.000 - 0,20  CPMK      
| Block Reward           | 50.000 - 0.15  CPMK           |
| Block Reward           | 150.000 - 0.10  CPMK           |
| Block Reward           | 300.000 - 0.08  CPMK           |
| Block Reward           | 700.000 - 0.50  CPMK           |


| Premine          | Q&A                  |
| ---------------------- |:-----------------------|
|Q: What is the premine in ChipMonk project?                  |A: Premine is 6,849,500 CPMK         |
|Q: Why did the team premine that amount?                  |A: ChipMonk World, a blockchain game, has a collection of 10.000 NFT players. Each of the players are divided in 11 different ranks and price level starting at 0.08 ETH up to 5.8 ETH. Each player once bought or owned has a redeem code for its CPMK equivalent to its ETH value spent.        |
|Q: What is the ratio formula for CPMK redeemable code?                  |A:  1 ETH spent on ChipMonk World player will lock 1.000 CPMK from the premine allocation. For example, if the player bought is 0.08 ETH, the owner will receive a redemption code for 80 CPMK        |
|Q:How can anyone redeem the CPMK?                  |A: To redeem your CPMK, first you need to buy your NFT player. Once you enter in the possession of the NFT you can access the redeemable code. The CPMK coins that are allocated to the NFT players are locked in the players account in the game.        |
|Q: How can one unlock the CPMK coins from his game account?                 |A: Each time an NFT player is engaged in a harvesting game, after the game is ended and the history of the game recorder in ChipMonk’s blockchain by the miners, a certain amount of the CPMK allocation is unlocked for spending.         |




License
-------

Chipmonk Core is released under the terms of the MIT license. See [COPYING](COPYING) for more
information or see https://opensource.org/licenses/MIT.
